package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HomePage {
    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".fa-shopping-bag");
    private final SelenideElement cartRedirectUrl = $("[data-icon=shopping-cart]");
    private final SelenideElement helpModal = $(".fa-question");
    private final SelenideElement loginModal = $(".modal-content");
    private final SelenideElement addBasketIcon = $("[data-icon=cart-plus]");
    private final SelenideElement sortMode = $(".container-fluid");
    private final SelenideElement cartIcon = $(".shopping-cart-icon");
    private final SelenideElement buildDemoShop2 = $(".nav-link");

    public void clickOnLogo(){
        logo.click();
    }


    public String greetingsMessage() {
        return greetings.getText();
    }

    public boolean isLogoDisplayed() {
        return logo.exists();
    }

    public String logoRedirectUrl() {
        return logo.parent().getAttribute("href");
    }

    public String cartRedirectUrl() {
        return cartRedirectUrl.getAttribute("href");
    }

    public boolean helpModal() {
        return helpModal.exists();
    }

    public boolean loginModal() {
        return loginModal.exists();
    }

    public boolean sortMode() {
        return sortMode.exists();
    }

    public boolean cartIcon() {
        return cartIcon.exists();
    }

    }