package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
public class DemoShopTest {

    private static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    HomePage homePage;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        homePage = new HomePage();
    }

    //@AfterMethod
    public void cleanUp() {
        homePage.clickOnLogo();
    }

    @Test
    public void verify_favourites_icon() {
        SelenideElement favouritesIcon = $(".navbar-nav [data-icon=heart]");
        assertTrue(favouritesIcon.exists(), "Favorites icon is displayed");
    }

    @Test
    public void verify_favourites_button() {
        SelenideElement favouritesIcon = $(".navbar-nav [data-icon=heart]");
        assertTrue(favouritesIcon.exists(), "Favourites icon is displayed");
        favouritesIcon.click();
        String wishListURL = favouritesIcon.parent().getAttribute("href");
        assertEquals(wishListURL, HOME_PAGE + "#/wishlist",
                "Page redirected to WishList Page");
    }

    @Test
    public void validate_greetings_message() {
        HomePage homePage = new HomePage();
        String message = homePage.greetingsMessage();
        assertEquals(message, "Hello guest!", "Verify the greetings message is Hello guest!");
    }

    @Test
    public void verify_homePage_logo() {
        HomePage homePage = new HomePage();
        assertTrue(homePage.isLogoDisplayed(), "Logo element exist in navigation bar");
        assertEquals(homePage.logoRedirectUrl(), HOME_PAGE, "Clicking the Logo redirects to homepage");
    }

    @Test
    public void favourites_icon_updates_upon_adding_a_product_to_the_favourites() {
        SelenideElement secondProduct = $("[href='#/product/3']");
        SelenideElement mainProductCard = secondProduct.parent().parent();
        SelenideElement favouritesButton = mainProductCard.$("[data-icon*=heart]");
        favouritesButton.click();
        assertEquals(favouritesButton.getAttribute("data-icon"), "heart-broken",
                "After adding a product to favourites, the favourites icon crashes");
        SelenideElement favouritesIcon = $("[data-icon=heart]~.shopping_cart_badge");
        assertEquals(favouritesIcon.getText(), "1",
                "After adding a product to favourites the favourites badge updates");
    }

    @Test
    public void verify_question_button_open_help_modal() {
        SelenideElement questionIcon = $(".fa-question");
        assertTrue(questionIcon.exists(), "Question icon is displayed");
        questionIcon.click();
        SelenideElement helpModal = $(".modal-content");
        assertTrue(helpModal.isDisplayed(),
                "After clicking on help button, help modal is displayed");
        SelenideElement close = $(".close");
        close.click();
    }

    @Test
    public void verify_login_button_opens_login_modal() {
        SelenideElement loginIcon = $(".fa-sign-in-alt");
        assertTrue(loginIcon.exists(), "Login icon is displayed");
        loginIcon.click();
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.exists(),
                "After clicking on login button, login modal is displayed");
        assertTrue(loginModal.isDisplayed(), "Login Modal is displayed");
        SelenideElement close = $(".close");
        close.click();
    }

    @Test
    public void verify_set_sort_mode_products() {
        homePage.clickOnLogo();
        ElementsCollection allProducts = $$(".card");
        SelenideElement firstProduct = allProducts.first();
        SelenideElement sortMode = $(".sort-products-select");
        assertTrue(sortMode.isDisplayed(), "Sort modes is displayed");
        sortMode.click();
        assertTrue(sortMode.isDisplayed(), "Sort type is displayed");

    }

    @Test
    public void verify_cartIcon() {
        SelenideElement cartIcon = $(".shopping-cart-icon");
        assertTrue(cartIcon.exists(), "Functional cart button");
        assertTrue(cartIcon.isDisplayed(), "Cart icon is displayed on page");

    }


    @Test
    public void verify_date_build_demoShop() {
        SelenideElement buildDemoShop = $(".nav-link");
        String buildVersion = buildDemoShop.getText();
        assertTrue(buildDemoShop.exists(), "Check if the site creation button appears on the site");
        assertEquals(buildVersion, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
        assertTrue(buildDemoShop.isDisplayed());
    }


    @Test
    public void favourites_icon_updates_upon_adding_two_product_to_the_favourites() {
        SelenideElement undo = $("[data-icon=undo]");
        undo.click();
        SelenideElement secondProduct = $("[href='#/product/3']");
        SelenideElement mainProductCard = secondProduct.parent().parent();
        SelenideElement favouritesButton = mainProductCard.$("[data-icon*=heart]");
        favouritesButton.click();
        assertEquals(favouritesButton.getAttribute("data-icon"), "heart-broken",
                "After adding a product to favourites, the favourites icon crashes");
        SelenideElement favouritesIcon = $("[data-icon=heart]~.shopping_cart_badge");
        assertEquals(favouritesIcon.getText(), "1",
                "After adding a product to favourites the favourites badge updates");
        SelenideElement mainProductCard2 = secondProduct.parent().parent();
        SelenideElement favouritesButton2 = mainProductCard2.$("[data-icon*=heart]");
        favouritesButton2.click();
        assertEquals(favouritesButton2.getAttribute("data-icon"), "heart",
                "After adding a product to favourites, the favourites icon crashes");
    }

    @Test
    public void verify_login_button_with_incorrect_user_and_correct_password() {
        SelenideElement loginButton = $(".fa-sign-in-alt");
        assertTrue(loginButton.exists(), "It's not working logging.");
        assertTrue(loginButton.isEnabled(), "Error message: Invalid user");
        assertTrue(loginButton.isDisplayed(), "Login is displayed");
        loginButton.click();
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.isDisplayed(), "Login Modal is displayed");
        SelenideElement submitLogin = $("button[type=submit]");
        assertTrue(submitLogin.isDisplayed(), "Submit Login is displayed");
        submitLogin.click();
        SelenideElement errorMessage = $("p.error");
        assertTrue(errorMessage.isDisplayed(), "Error messge is displayed");
        assertEquals(errorMessage.getText(), "Please fill in the username!");
        SelenideElement close = $(".close");
        close.click();

    }

    @Test
    public void verify_login_button_with_correct_user_and_correct_password() {
        SelenideElement loginButton = $(".fa-sign-in-alt");
        assertTrue(loginButton.exists(), "Successful login.");
        loginButton.click();
        assertTrue(loginButton.isDisplayed(),
                "After login with correct user and password you will successfully login");
        SelenideElement close = $(".close");
        close.click();

    }

    @Test
    public void verify_check_the_number_of_products_added_to_favourites() {
        SelenideElement checkNumber = $(".shopping-cart-icon");
        assertTrue(checkNumber.exists(), "View number of products added to favourites");
        checkNumber.click();
        assertTrue(checkNumber.isDisplayed(), "shopping-cart-icon-fa-layers-fa-fw");
    }
}
