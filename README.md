# automation-demo-shop

A brief presentation of my project.

## Getting started

This is the final project for Oana, within the FastTrackIt Test Automation course.
Software engineer: Oana Adina HENDEA


Tech stack used:
- Java17
- Maven
- Selenide Framework
- PageObject Models

### How to run the test
-git clone https://gitlab.com/OANAADINA/automation-demo-shop.git
Execute the following commands to:
- 'mvn clean test' -> Execute all test
- 'mvn allure:report' -> Generate Report
- 'mvn allure:serve' -> Open and present report

## Test implemented

| Test name                                             | Execution status | Date     |
|-------------------------------------------------------|------------------|----------|
| Home page logo                                        | Passed           | 09.04.22 |
| Cart icon                                             | Passed           | 09.04.22 |
| Favourites icon                                       | Passed           | 09.04.22 |
| Greetings message                                     | Passed           | 09.04.22 |
| Favourites button                                     | Passed           | 09.04.22 |
| Date build DemoShop                                   | Passed           | 09.04.22 |
| Login button opens login modal                        | Passed           | 09.04.22 |
| Login button with correct user and incorrect password | Passed           | 09.04.22 |
| Set sort mode products                                | Passed           | 09.04.22 |
| Login button with incorrect user and correct password | Passed           | 09.04.22 |
| Question button open help modal                       | Passed           | 09.04.22 |
| Check the number of products add to favourites        | Passed           | 09.04.22 |
| Icon updates upon adding a product to the favourites  | Passed           | 09.04.22 |
| Icon updates two product to the favourites            | Passed           | 09.04.22 |  
|                                                       |                  |          |  

### Reporting is also available in the Allure reports folder.
### Test are executed using Maven.
